## How install Project

1. После клонирования репозитория создать файл .env на основе .env.example . Добавить в него свои данные БДю
2. Выполнить команды `composer update` и `npm install` . После этого выполнить команду `php artisan key:generate`
   (она сгенерирует уникальный ключ для работы приложения).
   Далее необходимо выполнить команду `php artisan storage:link` для добавления ссылки на хранилище с документами
3. Создадим таблицы при помощи миграций и наполним их тестовыми данными: `php artisan migrate:fresh --seed`.
4. Пользуемся, тестируем)

// Чтобы добавить изображение студента по умолчанию - загрузите его в /storage/app/public/images/students/default-student.png