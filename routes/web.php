<?php

use App\Http\Controllers\GroupCourseWithTeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentGroupController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\TeacherController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resource('courses', CourseController::class)->except('show');
Route::resource('teachers', TeacherController::class)->except('show');
Route::resource('student-groups', StudentGroupController::class)->except('show');
Route::resource('students', StudentController::class)->except('show');

Route::resource('group-course-with-teachers', GroupCourseWithTeacherController::class)->except('show')->except('create')->except('edit');
Route::post('group-course-with-teachers/getCourses', [GroupCourseWithTeacherController::class, 'getCoursesAjax'])->name('getCoursesAjax');
Route::get('group-course-with-teachers/create/{studentGroup}/{course}', [GroupCourseWithTeacherController::class, 'create'])->name('group-course-with-teachers.create');
Route::get('group-course-with-teachers/edit/{groupCourseWithTeacher}/{studentGroup}/{course}', [GroupCourseWithTeacherController::class, 'edit'])->name('group-course-with-teachers.edit');
