<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Teacher;
use Illuminate\Database\Seeder;

class CourseTeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = Course::all();
        Teacher::all()->each(function ($teacher) use ($courses) {
            $teacher->courses()->attach($courses->random(rand(1,2)));
        });
    }
}
