<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\CourseTeacher;
use App\Models\GroupCourse;
use App\Models\Student;
use App\Models\StudentGroup;
use App\Models\Teacher;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Course::factory(20)->create();
        Teacher::factory(12)->create();
        StudentGroup::factory(10)->create();
        Student::factory(120)->create();
        $this->call(StatusSeeder::class);
        $this->call(CourseTeacherSeeder::class);
        $this->call(CourseStudentGroupSeeder::class);
    }
}
