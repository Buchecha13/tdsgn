<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\StudentGroup;
use Illuminate\Database\Seeder;

class CourseStudentGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = Course::all();
        StudentGroup::all()->each(function ($group) use ($courses) {
            $group->courses()->attach($courses->random(5));
        });
    }
}
