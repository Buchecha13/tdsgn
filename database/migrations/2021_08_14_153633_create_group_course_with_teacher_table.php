<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupCourseWithTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_course_with_teacher', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('course_student_group_id');
            $table->foreign('course_student_group_id')->references('id')->on('course_student_group');
            $table->foreignId('teacher_id')->constrained();
            $table->foreignId('status_id')->default(1)->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_course_with_teacher');
    }
}
