<?php

namespace Database\Factories;

use App\Models\GroupCourseWithTeacher;
use Illuminate\Database\Eloquent\Factories\Factory;

class GroupCourseWithTeacherFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = GroupCourseWithTeacher::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
