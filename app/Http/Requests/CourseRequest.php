<?php

namespace App\Http\Requests;

use App\Models\StudentGroup;
use App\Models\Teacher;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CourseRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $teachersTableName = (new Teacher())->getTable();
        $studentGroupsTableName = (new StudentGroup())->getTable();
        return [
            'teachers' => "array|exists:$teachersTableName,id",
            'studentGroups' => "array|exists:$studentGroupsTableName,id",
            'name' => [
                'required',
                'max:30',
                'min:3',
                Rule::unique('courses', 'name')->ignore($this->course)
            ]
        ];
    }
}
