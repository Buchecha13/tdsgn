<?php

namespace App\Http\Requests;

use App\Models\Course;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TeacherRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $CoursesTableName = (new Course())->getTable();
        return [
            'courses' => "array|exists:$CoursesTableName,id",
            'name' => [
                'required',
                Rule::unique('teachers', 'name')->ignore($this->teacher)
            ]
        ];
    }
}
