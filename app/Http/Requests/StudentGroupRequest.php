<?php

namespace App\Http\Requests;

use App\Models\Course;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentGroupRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $CoursesTableName = (new Course())->getTable();
        return [
            'courses' => "array|exists:$CoursesTableName,id",
            'name' => [
                'required',
                'min:3',
                'max:15',
                Rule::unique('student_groups', 'name')->ignore($this->student_group)
            ]
        ];
    }
}
