<?php

namespace App\Http\Requests;

use App\Models\StudentGroup;
use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tableNameStudentGroup = (new StudentGroup())->getTable();
        return [
            'name' => ['required', 'min:2', 'max:30', ''],
            'student_group_id' => "required|exists:{$tableNameStudentGroup},id",
            'photo' => 'mimes:jpeg,png|max:10000'
        ];
    }
}
