<?php

namespace App\Http\Requests;

use App\Models\Course;
use App\Models\CourseStudentGroup;
use App\Models\Status;
use App\Models\StudentGroup;
use App\Models\Teacher;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GroupCourseWithTeacherRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $courseStudentGroupTableName = (new CourseStudentGroup())->getTable();
        $teachersTableName = (new Teacher())->getTable();
        $statusesTableName = (new Status())->getTable();


        return [
            'course_student_group_id' => "exists:$courseStudentGroupTableName,id",
            'teacher_id' => "exists:$teachersTableName,id",
            'status_id' => "exists:$statusesTableName,id",
        ];
    }
}
