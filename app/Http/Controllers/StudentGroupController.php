<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentGroupRequest;
use App\Models\Course;
use App\Models\Student;
use App\Models\StudentGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class StudentGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('student_groups.index', [
            'studentGroups' => StudentGroup::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(StudentGroup $studentGroup)
    {
        return view('student_groups.create', [
            'studentGroup' => $studentGroup,
            'courses' => Course::all()->sortBy('name'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(StudentGroupRequest $request, StudentGroup $group)
    {
        $request->validated();
        //Create new Teacher
        $group->fill($request->except(['courses']))->save();
        //Courses attach
        if ($request->has('courses')) {
            $group->courses()->attach($request->input('courses'));
        }

        return redirect()->route('student-groups.edit', $group)->with('notification', 'Группа создана');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\StudentGroup $studentGroup
     */
    public function edit(StudentGroup $studentGroup)
    {
        return view('student_groups.create', [
            'studentGroup' => $studentGroup,
            'courses' => Course::all()->sortBy('name'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\StudentGroup $studentGroup
     */
    public function update(StudentGroupRequest $request, StudentGroup $studentGroup)
    {
        $request->validated();
        //Update current Group
        $studentGroup->fill($request->except(['courses']))->save();
        //Courses and Students detach
        $studentGroup->courses()->detach();
        //Courses attach
        if ($request->has('courses')) {
            $studentGroup->courses()->attach($request->input('courses'));
        }

        return redirect()->route('student-groups.edit', $studentGroup)->with('notification', 'Группа изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\StudentGroup $studentGroup
     */
    public function destroy(StudentGroup $studentGroup)
    {
        try {
            $studentGroup->delete();
        } catch (QueryException $e) {
            if ($e->errorInfo[1] == 1451) {
                return redirect()->route('student-groups.index')
                    ->with('alert', "Нельзя удалить группу, так как она привязана к курсам, или в ней есть студенты");
            }
        }
        return redirect()->route('student-groups.index')
            ->with('notification', 'Группа удалена');
    }
}
