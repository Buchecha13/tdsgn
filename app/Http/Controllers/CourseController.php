<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseRequest;
use App\Models\Course;
use App\Models\StudentGroup;
use App\Models\Teacher;
use Illuminate\Database\QueryException;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('courses.index', [
            'courses' => Course::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Course $course)
    {
        return view('courses.create', [
            'course' => $course,
            'teachers' => Teacher::all()->sortBy('name'),
            'studentGroups' => StudentGroup::all()->sortBy('name'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(CourseRequest $request, Course $course)
    {
        $request->validated();
        //Create new Course
        $course->fill($request->except(['teachers', 'studentGroups']))->save();
        //Teachers and Groups attach
        if ($request->has('teachers')) {
            $course->teachers()->attach($request->input('teachers'));
        }
        if ($request->has('studentGroups')) {
            $course->studentGroups()->attach($request->input('studentGroups'));
        }

        return redirect()->route('courses.edit', $course)->with('notification', 'Курс создан');
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(Course $course)
    {
        return view('courses.create', [
            'course' => $course,
            'teachers' => Teacher::all(),
            'studentGroups' => StudentGroup::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Course $course
     */
    public function update(CourseRequest $request, Course $course)
    {
        $request->validated();

        //Update current Course
        $course->fill($request->except(['teachers', 'studentGroups']))->save();
        //Teachers and Groups detach
        $course->teachers()->detach();
        $course->studentGroups()->detach();
        //Teachers and Groups attach
        if ($request->has('teachers')) {
            $course->teachers()->attach($request->input('teachers'));
        }
        if ($request->has('studentGroups')) {
            $course->studentGroups()->attach($request->input('studentGroups'));
        }

        return redirect()->route('courses.edit', $course)->with('notification', 'Курс изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Course $course
     */
    public function destroy(Course $course)
    {
        try {
            $course->delete();
        } catch (QueryException $e) {
            if ($e->errorInfo[1] == 1451) {
                return redirect()->route('courses.index')->with('alert', "Нельзя удалить курс, так как к нему привязаны группы, или преподаватели");
            }
        }
        return redirect()->route('courses.index')->with('notification', "Новость $course->id удалена");
    }
}
