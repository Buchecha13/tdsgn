<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupCourseWithTeacherRequest;
use App\Models\Course;
use App\Models\CourseStudentGroup;
use App\Models\GroupCourseWithTeacher;
use App\Models\Status;
use App\Models\StudentGroup;
use Illuminate\Http\Request;

class GroupCourseWithTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('group_course_with_teacher2.index', [
            'groups' => StudentGroup::all(),
            'courses' => Course::all(),
            'groupCourseWithTeacher' => GroupCourseWithTeacher::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(StudentGroup $studentGroup, Course $course, GroupCourseWithTeacher $groupCourseWithTeacher)
    {
        $courseStudentGroupId = CourseStudentGroup::query()
            ->where('student_group_id', $studentGroup->id)
            ->where('course_id', $course->id)->first();

        return view('group_course_with_teacher2.create', [
            'studentGroup' => $studentGroup,
            'course' => $course,
            'courseTeachers' => $course->teachers,
            'groupCourseWithTeacher' => $groupCourseWithTeacher,
            'statuses' => Status::all(),
            'courseStudentGroupId' => $courseStudentGroupId
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(GroupCourseWithTeacherRequest $request, GroupCourseWithTeacher $groupCourseWithTeacher)
    {
        $request->validated();
        //Create new
        $groupCourseWithTeacher->fill($request->except(['student_group_id', 'course']))->save();

        return redirect()->route('group-course-with-teachers.index')->with('notification', 'Связь назначена');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     */
    public function edit(GroupCourseWithTeacher $groupCourseWithTeacher, StudentGroup $studentGroup, Course $course)
    {
        $courseStudentGroupId = CourseStudentGroup::query()
            ->where('student_group_id', $studentGroup->id)
            ->where('course_id', $course->id)->first();

        return view('group_course_with_teacher2.create', [
            'studentGroup' => $studentGroup,
            'course' => $course,
            'courseTeachers' => $groupCourseWithTeacher->courseStudentGroup->course->teachers,
            'groupCourseWithTeacher' => $groupCourseWithTeacher,
            'statuses' => Status::all(),
            'courseStudentGroupId' => $courseStudentGroupId,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(GroupCourseWithTeacherRequest $request, GroupCourseWithTeacher $groupCourseWithTeacher)
    {
        $request->validated();

        $groupCourseWithTeacher->fill($request->except(['student_group_id', 'course']))->save();

        return redirect()->route('group-course-with-teachers.index')->with('notification', 'Связь обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     */
    public function destroy(GroupCourseWithTeacher $groupCourseWithTeacher)
    {
        $groupCourseWithTeacher->delete();
        return redirect()->route('group-course-with-teachers.index')
            ->with('notification', 'Запись удалена');
    }

    /**
     * Обработчик асинхронного запроса
     */
    public function getCoursesAjax(Request $request)
    {
        $studentGroup = StudentGroup::query()->where('id', $request->groupId)->first();
        ;
        return view('group_course_with_teacher2.blocks.group_course', [
            'courses' => $studentGroup->courses,
            'groupCourseWithTeacher' => GroupCourseWithTeacher::all(),
            'group' => $studentGroup,
        ]);
    }
}
