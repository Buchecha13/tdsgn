<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Models\Course;
use App\Models\Student;
use App\Models\StudentGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $students = Student::paginate(20);
        return view('students.index', [
            'students' => $students
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Student $student)
    {
        return view('students.create', [
            'student' => $student,
            'studentGroups' => StudentGroup::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(StudentRequest $request, Student $student)
    {
        $request->validated();
        //Photo handler
        if ($request->file('photo')) {
            $path = Storage::putFileAs('public/images/students', $request->file('photo'), $request->file('photo')->getClientOriginalName());
            $url = Storage::url($path);

            $student->photo = $url;
        }
        //Create new Student
        $student->fill($request->except('photo'))->save();

        return redirect()->route('students.edit', $student)->with('notification', 'Студент создан');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Student $student
     */
    public function edit(Student $student)
    {
        return view('students.create', [
            'student' => $student,
            'studentGroups' => StudentGroup::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Student $student
     */
    public function update(StudentRequest $request, Student $student)
    {
        $request->validated();
        //Photo handler
        if ($request->file('photo')) {
            $path = Storage::putFileAs('public/images/students', $request->file('photo'), $request->file('photo')->getClientOriginalName());
            $url = Storage::url($path);

            $student->photo = $url;
        }
        //Update current Student
        $student->fill($request->except('photo'))->save();

        return redirect()->route('students.edit', $student)->with('notification', 'Студент изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Student $student
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('students.index', $student)->with('notification', 'Студент удален');
    }
}
