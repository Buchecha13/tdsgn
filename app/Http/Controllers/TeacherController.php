<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeacherRequest;
use App\Models\Course;
use App\Models\StudentGroup;
use App\Models\Teacher;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('teachers.index', [
            'teachers' => Teacher::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Teacher $teacher)
    {
        return view('teachers.create', [
            'teacher' => $teacher,
            'courses' => Course::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(TeacherRequest $request, Teacher $teacher)
    {
        $request->validated();
        //Create new Teacher
        $teacher->fill($request->except(['courses']))->save();
        //Courses attach
        if ($request->has('courses')) {
            $teacher->courses()->attach($request->input('courses'));
        }

        return redirect()->route('teachers.edit', $teacher)->with('notification', 'Преподаватель создан');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Teacher $teacher
     */
    public function edit(Teacher $teacher)
    {
        return view('teachers.create', [
            'teacher' => $teacher,
            'courses' => Course::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Teacher $teacher
     */
    public function update(TeacherRequest $request, Teacher $teacher)
    {
        $request->validated();
        //Update current Teacher
        $teacher->fill($request->except(['courses']))->save();
        //Courses detach
        $teacher->courses()->detach();
        //Courses attach
        if ($request->has('courses')) {
            $teacher->courses()->attach($request->input('courses'));
        }

        return redirect()->route('teachers.edit', $teacher)->with('notification', 'Преподаватель изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Teacher $teacher
     */
    public function destroy(Teacher $teacher)
    {
        try {
            $teacher->delete();
        } catch (QueryException $e) {
            if ($e->errorInfo[1] == 1451) {
                return redirect()->route('teachers.index')
                    ->with('alert', "Нельзя удалить преподавателя, так как к нему привязаны курсы");
            }
        }
        return redirect()->route('teachers.index')
            ->with('notification', 'Преподаватель удален');
    }
}
