<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseStudentGroup extends Model
{
    use HasFactory;

    protected $fillable = ['course_id', 'student_group_id'];

    protected $table = 'course_student_group';

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function studentGroup()
    {
        return $this->belongsTo(StudentGroup::class, 'student_group_id');
    }
}
