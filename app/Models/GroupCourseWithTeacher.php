<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupCourseWithTeacher extends Model
{
    protected $fillable = ['course_student_group_id', 'teacher_id', 'status_id'];
    protected $table = 'group_course_with_teacher';

    use HasFactory;

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function courseStudentGroup()
    {
        return $this->belongsTo(CourseStudentGroup::class, 'course_student_group_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

}
