<li class="nav-item"><a class="nav-link" href="{{ route('courses.index') }}">Курсы</a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('teachers.index') }}">Преподаватели</a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('student-groups.index') }}">Группы</a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('students.index') }}">Студенты</a></li>
<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <b> Добавить </b>
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('courses.create') }}">Курс</a>
        <a class="dropdown-item" href="{{ route('teachers.create') }}">Преподавателя</a>
        <a class="dropdown-item" href="{{ route('student-groups.create') }}">Группу</a>
        <a class="dropdown-item" href="{{ route('students.create') }}">Студента</a>
    </div>
</li>
<li class="nav-item"><a class="nav-link" href="{{ route('group-course-with-teachers.index') }}">Назначение преподавателя</a></li>


