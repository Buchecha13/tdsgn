@extends('layouts.main')

@section('title', 'Преподаватели')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card pt-3">
                <h1 class="text-center mb-3">Преподаватели</h1>

                <table class="table table-striped text-center">
                    <tr>
                        <td><b>№</b></td>
                        <td><b>Имя преподавателя</b></td>
                        <td><b>Какие курсы может вести</b></td>
                        <td><b>Действия</b></td>
                    </tr>
                    @php ($i = 1)
                    @forelse($teachers as $teacher)
                        <tr>
                            <td style="vertical-align: middle"> {{ $i++ }} </td>
                            <td style="vertical-align: middle">{{ $teacher->name }}</td>
                            <td style="vertical-align: middle">
                                @forelse($teacher->courses as $course)
                                    {{ $course->name  }} <br>
                                @empty
                                    <span style="color: #b91d19">Курсы не определены</span>
                                @endif
                            </td>
                            <td style="vertical-align: middle">
                                <ul class="nav justify-content-center">
                                    <li class="mr-3">
                                        <a href="{{ route('teachers.edit', $teacher) }}" class="btn btn-primary">
                                            <i class="bi bi-pencil-square" style="font-size: 1rem"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form method="post" action="{{ route('teachers.destroy', $teacher) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">
                                                <i class="bi bi-trash" style="font-size: 1rem"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @empty

                    @endforelse
                </table>
            </div>
        </div>
    </div>
@endsection

