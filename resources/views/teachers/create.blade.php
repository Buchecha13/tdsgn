@extends('layouts.main')
@section('title', 'Преподаватель')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card p-3">
                    <h1 class="text-center mb-3">@if(!$teacher->id)Создание преподавателя@elseИзменение преподавателя@endif</h1>

                    <div class="card-body">
                        <form method="post"
                              action="@if(!$teacher->id) {{ route('teachers.store') }} @else {{ route('teachers.update', $teacher) }} @endif"
                              enctype="multipart/form-data">
                            @csrf
                            @if($teacher->id) @method('PUT')@endif

                            <div class="form-group">
                                <label for="newsTitle"><h3>Имя преподавателя</h3></label>
                                @error('name')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <input class="form-control col-md-6" id="newsTitle" type="text" name="name"
                                       value="{{ old('name') ?? $teacher->name }}">
                            </div>
                            <hr>
                            <h3>Какие курсы может преподавать</h3>
                            @error('courses')
                            <div class="alert alert-danger col-md-6">{{ $message }}</div>
                            @enderror
                            <div class="form-check d-flex flex-wrap">
                                @forelse($courses as $course)
                                    <div class="mr-5">
                                        <input class="form-check-input" type="checkbox" name="courses[]" multiple
                                               value="{{ $course->id }}"
                                               id="{{$course->id . '-' . $course->name}}"
                                               @if($teacher->courses->contains('id', $course->id)) checked="" @endif>
                                        <label
                                            for="{{$course->id . '-' . $course->name}}">{{ $course->name }}</label><br>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <hr>
                            <div class="form-group">
                                <input class="btn btn-outline-primary" type="submit"
                                       value="@if($teacher->id)Изменить@elseСоздать@endif преподавателя">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
