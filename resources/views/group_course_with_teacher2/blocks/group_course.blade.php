@foreach($courses as $course)
    @php
        $courseStudentGroup = \App\Models\CourseStudentGroup::query()
            ->where('student_group_id', $group->id)
            ->where('course_id', $course->id)->first();
        $groupCourseWithTeacherItem = \App\Models\GroupCourseWithTeacher::query()
            ->where('course_student_group_id', $courseStudentGroup->id)->first();
    @endphp
    <tr>
        <td>{{ $course->id }}</td>
        <td>{{ $course->name }}</td>
        <td>
                @if(!is_null($groupCourseWithTeacherItem))
                    {{ $groupCourseWithTeacherItem->teacher->name }}
                @endif
        </td>
        <td>
            @if(!is_null($groupCourseWithTeacherItem))
                {{ $groupCourseWithTeacherItem->status->name }}
            @endif
        </td>
        <td>
            @if(!is_null($groupCourseWithTeacherItem))
                <ul class="nav justify-content-center">
                    <a href="{{ route('group-course-with-teachers.edit',['groupCourseWithTeacher' => $groupCourseWithTeacherItem, 'studentGroup' => $group, 'course' => $course]) }}"
                       class="btn btn-primary mr-3" title="Указать преподавателя и статус">
                        <i class="bi bi-pencil-square" style="font-size: 1rem"></i>
                    </a>

                    <form method="post" action="{{ route('group-course-with-teachers.destroy', $groupCourseWithTeacherItem) }}">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">
                            <i class="bi bi-trash" style="font-size: 1rem"></i>
                        </button>
                    </form>
                </ul>
            @else
                <ul class="nav justify-content-center">
                    <a href="{{ route('group-course-with-teachers.create',['groupCourseWithTeacher' => $groupCourseWithTeacherItem, 'studentGroup' => $group, 'course' => $course]) }}"
                       class="btn btn-primary" title="Создать преподавателя и статус">
                        <i class="bi bi-pencil-square" style="font-size: 1rem"></i>
                    </a>
                </ul>
            @endif
        </td>
    </tr>
@endforeach
