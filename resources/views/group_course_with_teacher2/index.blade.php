@extends('layouts.main')

@section('title', 'Группы')
{{--@dd($groupCourseWithTeacher[0]->courseStudentGroup->course->id)--}}
@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card pt-3">
                <h1 class="text-center mb-3">Группы</h1>
                @forelse($groups as $group)
                    <button class="btn btn-outline-info get-courses"
                            data-id="{{ $group->id }}" type="button"
                            data-toggle="collapse" data-target="#collapse{{ $group->id }}"
                            aria-expanded="false" aria-controls="collapse{{ $group->id }}">
                        {{ $group->name }}
                    </button>
                    <div class="collapse" id="collapse{{ $group->id }}">
                        <table class="table table-striped text-center">
                            <tr>
                                <td><b>№</b></td>
                                <td><b>Наименование курса</b></td>
                                <td><b>Преподаватель</b></td>
                                <td><b>Статус</b></td>
                                <td><b>Действия</b></td>
                            </tr>
                            <tbody id="{{ $group->id }}">

                            </tbody>

                        </table>
                    </div>
                    <hr>
                @empty
                @endforelse


            </div>
        </div>
    </div>

    <script src="{{ asset('js/getDataAjax.js') }}"></script>
@endsection


