@extends('layouts.main')
@section('title', 'Назначение')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card p-3">
                    <h1 class="text-center mb-3">@if(!$groupCourseWithTeacher->id)Назначение преподавателя на курс
                        конретной
                        группы@elseИзменение преподавателя на курс конретной
                        группы@endif</h1>
                    <div class="card-body">
                        <form method="post"
                              action="@if(!$groupCourseWithTeacher->id) {{ route('group-course-with-teachers.store') }} @else {{ route('group-course-with-teachers.update', $groupCourseWithTeacher) }} @endif"
                              enctype="multipart/form-data">
                            @csrf
                            @if($groupCourseWithTeacher->id) @method('PUT')@endif

                            @error('course_student_group_id')
                            <div class="alert alert-danger col-md-6">{{ $message }}</div>
                            @enderror
                            <input hidden value="{{ $courseStudentGroupId->id }}" name="course_student_group_id">

                            <div class="form-group">
                                <h3>№ группы</h3>
                                <input disabled class="form-control col-md-6" type="text"
                                       name="student_group"
                                       value="{{ $studentGroup->name }}">
                            </div>

                            <div class="form-group">
                                <h3>Название курса</h3>
                                <input disabled class="form-control col-md-6" type="text"
                                       name="course"
                                       value="{{ $course->name }}">
                            </div>

                            <div class="form-group">
                                <h3>Выбрать преподавателя</h3>
                                @error('teacher_id')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <select class="form-control col-md-3" name="teacher_id">
                                    @forelse($courseTeachers as $teacher)
                                        <option
                                            @if($teacher->id == $groupCourseWithTeacher->teacher_id)
                                            selected
                                            @endif
                                            value="{{ $teacher->id }}">{{ $teacher->name }}
                                        </option>
                                    @empty
                                        <option selected value="{{ null }}">Нет преподавателей
                                        </option>
                                    @endforelse
                                </select>
                            </div>

                            <div class="form-group">
                                <h3>Назначить статус</h3>
                                @error('status_id')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <select class="form-control col-md-3" name="status_id">
                                    @foreach($statuses as $status)
                                        <option
                                            @if($status->id == $groupCourseWithTeacher->status_id)
                                            selected
                                            @endif
                                            value="{{ $status->id }}">{{ $status->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="btn btn-outline-primary" type="submit"
                                       value="@if ($groupCourseWithTeacher->id){{__('Изменить')}}@else{{__('Добавить')}}@endif связь">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/getDataAjax.js') }}"></script>
@endsection

