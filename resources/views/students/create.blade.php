@extends('layouts.main')
@section('title', 'Преподаватель')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card p-3">
                    <h1 class="text-center mb-3">@if(!$student->id)Создание студента@elseИзменение
                        студента@endif</h1>

                    <div class="card-body">
                        <form method="post"
                              action="@if(!$student->id) {{ route('students.store') }} @else {{ route('students.update', $student) }} @endif"
                              enctype="multipart/form-data">
                            @csrf
                            @if($student->id) @method('PUT')@endif

                            <div class="form-group">
                                <label for="newsTitle"><h3>Имя студента</h3></label>
                                @error('name')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <input class="form-control col-md-6" id="newsTitle" type="text" name="name"
                                       value="{{ old('name') ?? $student->name }}">
                            </div>
                            <hr>
                            <h3>В какой группе</h3>
                            @error('groups')
                            <div class="alert alert-danger col-md-6">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <select class="form-control col-md-3" name="student_group_id">
                                    @foreach($studentGroups as $group)
                                        <option
                                            @if($group->id == $student->student_group_id)
                                            selected
                                            @endif
                                            value="{{ $group->id }}">{{ $group->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <h3>Фото студента</h3>
                            <div class="student-photo mb-3">
                                @error('photo')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <div class="custom-file col-6 mb-2">
                                    <input onchange="setFilename()" id="newsImage" name="photo" type="file"
                                           class="custom-file-input">
                                    <label
                                        class="custom-file-label">@if($student->photo){{ last(explode('/', $student->photo)) }} @else
                                            Выберите файл@endif</label>
                                </div>
                                <p>Желательно загружать изображение размером 120х120px</p>
                            @if($student->photo)
                                    <div class="news-img-item">
                                        <img width="120px" src="{{ asset($student->photo) }}"
                                             alt="{{ $student->name }}">
                                    </div>
                                @endif
                            </div>
                            <hr>
                            <div class="form-group">
                                <input class="btn btn-outline-primary" type="submit"
                                       value="@if($student->id)Изменить@elseСоздать@endif студента">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

<script type="text/javascript">
    function setFilename() {
        var fileName = document.getElementById("newsImage").files[0].name;
        var label = document.getElementsByClassName('custom-file-label');
        label[0].innerHTML = fileName;
    }
</script>
