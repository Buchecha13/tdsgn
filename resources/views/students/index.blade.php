@extends('layouts.main')

@section('title', 'Студенты')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card pt-3">
                <h1 class="text-center mb-3">Студенты</h1>

                <table class="table table-striped text-center">
                    <tr>
                        <td><b>ID</b></td>
                        <td><b>Имя стужента</b></td>
                        <td><b>В какой группе</b></td>
                        <td><b>Действия</b></td>
                    </tr>

                    @forelse($students as $student)
                        <tr>
                            <td style="vertical-align: middle"> {{ $student->id }} </td>
                            <td style="vertical-align: middle">{{ $student->name }}</td>
                            <td style="vertical-align: middle">{{$student->studentGroup->name}}</td>
                            <td style="vertical-align: middle">
                                <ul class="nav justify-content-center">
                                    <li class="mr-3">
                                        <a href="{{ route('students.edit', $student) }}" class="btn btn-primary">
                                            <i class="bi bi-pencil-square" style="font-size: 1rem"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form method="post" action="{{ route('students.destroy', $student) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">
                                                <i class="bi bi-trash" style="font-size: 1rem"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </table>
                {{ $students->links() }}
            </div>
        </div>
    </div>
@endsection

