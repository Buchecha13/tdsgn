@extends('layouts.main')

@section('title', 'Группы')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card pt-3">
                <h1 class="text-center mb-3">Группы</h1>

                <table class="table table-striped text-center">
                    <tr>
                        <td><b>№</b></td>
                        <td><b>Наименование группы</b></td>
                        <td><b>Кол-во студентов</b></td>
                        <td><b>Какие курсы должна изучать</b></td>
                        <td><b>Действия</b></td>
                    </tr>
                    @php ($i = 1)
                    @forelse($studentGroups as $studentGroup)
                        <tr>
                            <td style="vertical-align: middle"> {{ $i++ }} </td>
                            <td style="vertical-align: middle">{{ $studentGroup->name }}</td>
                            <td style="vertical-align: middle">{{ count($studentGroup->students) }}</td>
                            <td style="vertical-align: middle">
                                @forelse($studentGroup->courses as $course)
                                    {{ $course->name  }} <br>
                                @empty
                                    <span style="color: #b91d19">Курсы не определены</span>
                                @endif
                            </td>
                            <td style="vertical-align: middle">
                                <ul class="nav justify-content-center">
                                    <li class="mr-3">
                                        <a href="{{ route('student-groups.edit', $studentGroup) }}" class="btn btn-primary">
                                            <i class="bi bi-pencil-square" style="font-size: 1rem"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form method="post" action="{{ route('student-groups.destroy', $studentGroup) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">
                                                <i class="bi bi-trash" style="font-size: 1rem"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @empty

                    @endforelse
                </table>
            </div>
        </div>
    </div>
@endsection

