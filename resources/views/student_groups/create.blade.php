@extends('layouts.main')
@section('title', 'Группа')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card p-3">
                    <h1 class="text-center mb-3">@if(!$studentGroup->id)Создание группы@elseИзменение группы@endif</h1>

                    <div class="card-body">
                        <form method="post"
                              action="@if(!$studentGroup->id) {{ route('student-groups.store') }} @else {{ route('student-groups.update', $studentGroup) }} @endif"
                              enctype="multipart/form-data">
                            @csrf
                            @if($studentGroup->id) @method('PUT')@endif

                            <div class="form-group">
                                <label for="newsTitle"><h3>Наименование группы</h3></label>
                                @error('name')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <input class="form-control col-md-6" id="newsTitle" type="text" name="name"
                                       value="{{ old('name') ?? $studentGroup->name }}">
                            </div>
                            <hr>
                            <h3>Какие курсы должна изучать</h3>
                            @error('courses')
                            <div class="alert alert-danger col-md-6">{{ $message }}</div>
                            @enderror
                            <div class="form-check d-flex flex-wrap">
                                @forelse($courses as $course)
                                    <div class="mr-5">
                                        <input class="form-check-input" type="checkbox" name="courses[]" multiple
                                               value="{{ $course->id }}"
                                               id="{{$course->id . '-' . $course->name}}"
                                               @if($studentGroup->courses->contains('id', $course->id)) checked="" @endif>
                                        <label
                                            for="{{$course->id . '-' . $course->name}}">{{ $course->name }}</label><br>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <hr>

                            <h3>Студенты в группе</h3>
                            <div class="form-check d-flex flex-wrap">
                                <ul>
                                @forelse($studentGroup->students as $student)
                                        <li><a href="#">{{ $student->name }}</a></li>
                                @empty
                                    <span> Нет студентов </span>
                                @endforelse
                                </ul>
                            </div>
                            <hr>

                            <div class="form-group">
                                <input class="btn btn-outline-primary" type="submit"
                                       value="@if($studentGroup->id)Изменить@elseСоздать@endif группу">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
