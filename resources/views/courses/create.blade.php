@extends('layouts.main')
@section('title', 'Курс')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card p-3">
                    <h1 class="text-center mb-3">@if(!$course->id)Создание курса@elseИзменение курса@endif</h1>

                    <div class="card-body">
                        <form method="post"
                              action="@if(!$course->id) {{ route('courses.store') }} @else {{ route('courses.update', $course) }} @endif"
                              enctype="multipart/form-data">
                            @csrf
                            @if($course->id) @method('PUT')@endif

                            <div class="form-group">
                                <label for="newsTitle"><h3>Наименование курса</h3></label>
                                @error('name')
                                <div class="alert alert-danger col-md-6">{{ $message }}</div>
                                @enderror
                                <input class="form-control col-md-6" id="newsTitle" type="text" name="name"
                                       value="{{ old('name') ?? $course->name }}">
                            </div>
                            <hr>
                            <h3>Кто может преподавать</h3>
                            @error('teachers')
                            <div class="alert alert-danger col-md-6">{{ $message }}</div>
                            @enderror
                            <div class="form-check d-flex flex-wrap">
                                @forelse($teachers as $teacher)
                                    <div class="mr-5">
                                        <input class="form-check-input" type="checkbox" name="teachers[]" multiple
                                               value="{{ $teacher->id }}"
                                               id="{{$teacher->id . '-' . $teacher->name}}"
                                               @if($course->teachers->contains('id', $teacher->id)) checked="" @endif>
                                        <label
                                            for="{{$teacher->id . '-' . $teacher->name}}">{{ $teacher->name }}</label><br>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                            <hr>
                            <h3>В каких групах должен преподаваться</h3>
                            @error('studentGroups')
                            <div class="alert alert-danger col-md-6">{{ $message }}</div>
                            @enderror
                            <div class="form-check d-flex flex-wrap">
                                @foreach($studentGroups as $studentGroup)
                                    <div class="mr-5">
                                        <input class="form-check-input" type="checkbox" name="studentGroups[]" multiple
                                               value="{{ $studentGroup->id }}"
                                               id="{{$studentGroup->id . '-' . $studentGroup->name}}"
                                               @if($course->studentGroups->contains('id', $studentGroup->id)) checked="" @endif>
                                        <label
                                            for="{{$studentGroup->id . '-' . $studentGroup->name}}">{{ $studentGroup->name }}</label><br>
                                    </div>
                                @endforeach
                            </div>
                            <hr>
                            <div class="form-group">
                                <input class="btn btn-outline-primary" type="submit"
                                       value="@if($course->id)Изменить@elseСоздать@endif курс">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
