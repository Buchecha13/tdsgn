@extends('layouts.main')

@section('title', 'Курсы')
@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card pt-3">
                <h1 class="text-center mb-3">Курсы</h1>

                <table class="table table-striped text-center">
                    <tr>
                        <td><b>№</b></td>
                        <td><b>Название курса</b></td>
                        <td><b>Могут преподавать</b></td>
                        <td><b>В каких группах должен проходить</b></td>
                        <td><b>Дата создания</b></td>
                        <td><b>Действия</b></td>
                    </tr>
                    @php ($i = 1)
                    @forelse($courses as $course)
                        <tr>
                            <td style="vertical-align: middle"> {{ $i++ }} </td>
                            <td style="vertical-align: middle">{{ $course->name }}</td>
                            <td style="vertical-align: middle">
                                @forelse($course->teachers as $teacher)
                                    {{ $teacher->name  }} <br>
                                @empty
                                    <span style="color: #b91d19">Преподаватели не назначены</span>
                                @endif
                            </td>
                            <td style="vertical-align: middle">
                                @forelse($course->studentGroups as $group)
                                    {{ $group->name }} <br>
                                @empty
                                    <span style="color: #b91d19"> Не назначен ни одной группе </span>
                                @endif
                            </td>
                            <td style="vertical-align: middle">{{ $course->created_at ?? '00-00-000'}}</td>
                            <td style="vertical-align: middle">
                                <ul class="nav justify-content-center">
                                    <li class="mr-3">
                                        <a href="{{ route('courses.edit', $course) }}" class="btn btn-primary">
                                            <i class="bi bi-pencil-square" style="font-size: 1rem"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <form method="post" action="{{ route('courses.destroy', $course) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">
                                                <i class="bi bi-trash" style="font-size: 1rem"></i>
                                            </button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @empty

                    @endforelse
                </table>
            </div>
        </div>
    </div>
@endsection

