//Получаем курсы определенной группы
function getCourses(groupId) {
    (
        async () => {
            const response = await fetch('/group-course-with-teachers/getCourses', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                body: JSON.stringify({
                    groupId: groupId
                })
            });
            const answer = await response.text();
            document.getElementById(groupId).innerHTML = answer
        }
    )();
}

//Вешаем на каждую кнопку обработчик
let buttons = document.querySelectorAll('.get-courses');
buttons.forEach((elem) => {
    elem.addEventListener('click', () => {
        let groupId = elem.getAttribute('data-id');
        getCourses(groupId);
    });
});

